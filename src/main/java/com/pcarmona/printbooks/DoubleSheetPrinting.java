package com.pcarmona.printbooks;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Pablo Carmona A.
 */
public class DoubleSheetPrinting {

	private final int pageCount;
	private final int physicalPages;

	public DoubleSheetPrinting() {
		this.pageCount = 0;
		this.physicalPages = 0;
	}

	public DoubleSheetPrinting(int pageCount) {
		this.pageCount = pageCount;
		this.physicalPages = calculatePhysicalPages(pageCount);
	}

	public DoubleSheetPrinting(int pageCount, int physicalPages) {
		this.pageCount = pageCount;
		this.physicalPages = physicalPages;
	}

	public static int calculatePhysicalPages(int pageCount) {
		int m = pageCount / 4;

		if (pageCount % 4 > 0) {
			++m;
		}

		return m;
	}

	public int getPageCount() {
		return pageCount;
	}

	public int getPhysicalPages() {
		return physicalPages;
	}

	public int getLeftSideNumberForOddPages(int page) {
		return 2 * page - 1;
	}

	public int getRightSideNumberForOddPages(int page) {
		return 2 * page - 1 + 2 * physicalPages;
	}

	public int getLeftSideNumberForEvenPages(int page) {
		return 2 * page;
	}

	public int getRightSideNumberForEvenPages(int page) {
		return 2 * page + 2 * physicalPages;
	}

	private void addNumberPage(List<Integer> numbers, int page) {
		if (page <= pageCount) {
			numbers.add(page);
		}
	}

	public List<Integer> getOddPageNumbers(boolean invert) {
		List<Integer> numbers = new ArrayList<>();

		for (int n = 1; n <= physicalPages; ++n) {
			if (invert) {
				addNumberPage(numbers, getRightSideNumberForOddPages(n));
				addNumberPage(numbers, getLeftSideNumberForOddPages(n));
			} else {
				addNumberPage(numbers, getLeftSideNumberForOddPages(n));
				addNumberPage(numbers, getRightSideNumberForOddPages(n));
			}
		}

		return numbers;
	}

	public List<Integer> getEvenPageNumbers(boolean invert) {
		List<Integer> numbers = new ArrayList<>();

		for (int n = 1; n <= physicalPages; ++n) {
			if (invert) {
				addNumberPage(numbers, getRightSideNumberForEvenPages(n));
				addNumberPage(numbers, getLeftSideNumberForEvenPages(n));
			} else {
				addNumberPage(numbers, getLeftSideNumberForEvenPages(n));
				addNumberPage(numbers, getRightSideNumberForEvenPages(n));
			}
		}

		return numbers;
	}

}
