package com.pcarmona.printbooks;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class PrintBooks {

	public void printPages(String userInput) {
		DoubleSheetPrinting dsp = new DoubleSheetPrinting(Integer.parseInt(userInput));

		System.out.println("Document pages: " + dsp.getPageCount());
		System.out.println("Physical pages: " + dsp.getPhysicalPages());

		List<Integer> oddPages = dsp.getOddPageNumbers(false);
		long doubleOddPages = Math.round(oddPages.size() / 2.0);
		System.out.println("Odd pages (" + doubleOddPages + "): " + oddPages.stream()
				.map(p -> p.toString())
				.collect(Collectors.joining(",")));

		List<Integer> evenPages = dsp.getEvenPageNumbers(false);
		long doubleEvenPages = Math.round(evenPages.size() / 2.0);
		System.out.println("Even pages (" + doubleEvenPages + "): " + evenPages.stream()
				.map(p -> p.toString())
				.collect(Collectors.joining(",")));

		if (doubleOddPages != doubleEvenPages) {
			System.out.println("WARNING: You should add an extra empty page to the end of document!");
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		PrintBooks pb = new PrintBooks();

		if (args.length > 0) {
			pb.printPages(args[0]);
		} else {
			System.out.println("Enter document page number: ");
			pb.printPages(new Scanner(System.in).next());
		}
	}

}
